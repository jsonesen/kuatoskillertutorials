# Kuatos Killer Tutorials

Welcome to the collection of git, and programming lessons for KuatoLivesMF
and others who stumble upon this.

[Get started by reading lessons](lessons/gettinggoing.md)

If you want help or to collaborate with other folks on this I have a [discord](https://discord.gg/bH8yQM) server setup
