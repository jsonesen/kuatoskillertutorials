"""Lesson one challenge.

This is the very first challenge question I want you to solve. There is a
LOGIC error somewhere in this file, copy it to the solutions directory as
yourusername_one.py and solve it, then open a merge request back into the
project.

The script generates a fractal image  julia.pmg, it probably wont execute right
away due to the error. Remember, look for logic statements in the script and
try out the available operators to get it right ("or", "and", "not").

Don't let the math intimidate you, as the only relevant piece here is the logic
that is around the math, so all the numbers are right, but they may be in the
wrong spot (HINT: look in the setup code).

"""
import numpy

# Image width and height
w, h = 200, 200

# Specify real and imaginary range for image
realmin, realmax = -2.0, 2.0
imaginarymin, imaginarymax = -2.0, 2.0

# C is our complex number, pick a value
c = complex(0.0, 0.65)

# Generate evenly spaced values that cover both ranges,
# this effectively sets up the "canvas"
realrange = numpy.arange(realmin, realmax, (realmax - realmin) / w)
imaginaryrange = numpy.arange(imaginarymax, imaginarymin,
                              (imaginarymin - imaginarymax) / h)

# Create and open an output file and its header info
fout = open('julia.pgm', 'w')
fout.write('P2\n# Julia Set image\n{} {} \n255\n'.format(str(w), str(h)))

# Generate the pixels and write to file
for imaginarynumber in imaginaryrange:
    for realnumber in realrange:
        z = complex(realnumber, imaginarynumber)
        n = 255
        while abs(z) < 10 or n >= 5:
            z = z * z + c
            n -= 5
        # Write to file
        fout.write('{} '.format(str(n)))
    fout.write('\n')
fout.close()
