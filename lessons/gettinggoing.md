Introduction
=
One of the first things I learned about working remotely came in the form of github pull requests.
Later, I moved into GitLab as well. This came with a whole new set of problems to solve which were
not exactly covered in detail anywhere. As such, the following represents some of those lessons.

Gitlab SSH
=

Setting up your public key with GitLab is very helpful as you won't need to sign in manually for
each protected action as you do with http.

GitLab actually does provide a very in depth [tutorial](https://docs.gitlab.com/ee/ssh/README.html)
on setting this up. However, it is very verbose and may be intimidating to someone who does not
regularly read technical literature, let alone understand all the terms references. Below you will find something a bit more bite sized albeit lacks in depth explanation. But, honestly,
it's more fun to try it out with simple instructions and if theings blow up go rtfm. That said,
if you commit a day or two to thoroughly reading and comprehending that one
[page](https://docs.gitlab.com/ee/ssh/README.html) you will progress in a way that is 
pretty rewarding.

If you already have a key setup then it's just a matter of copying the contents of your
public key, which, by default is `~/.ssh/id_rsa.pub`.

If I am adding my key somewhere I prefer to get the key from the terminal:
 - `cat ~/.ssh/id_rsa.pub`
 - Then just copy and paste the output into the provided field on gitlab

Generating one takes a few more steps but is pretty easy:
 - `ssh-keygen -o -t rsa -b 4096 -C "email@example.com"`
 - you will be prompted to create a password, it can be skipped. Best practices dictate you 
   create one but that's up to you.
 - `cat ~/.ssh/id_rsa.pub`
 - copy/paste to gitlab!
 - `ssh -T git@gitlab.com` to test it.

Tutorial
=
From you terminal, if you haven't already, fork this repository by navigating to the project page and clicking the fork icon in the upper right hand corner of the project area of the page.

- Then you will want to clone your fork to your machine:`git clone (url to your fork)`
- Create a branch on your local checkout:`git branch (your username)`
- Then check it out to begin working on the branch you created:`git checkout (branch name)`

This will be your very own branch where I want you to upload solutions to the challenges existing in the tutorials.
The point of this is to begin learning about ways to separate your work on a shared code base in a way that does not
introduce potential issues into the main branch used for building/deploying your software (traditionally known as "master").

Congrats! Hopefully, you now have a fork of this project as well as your very own branch to work through this with.


[Finally, go checkout the challenge](challenges/one.py)
=



